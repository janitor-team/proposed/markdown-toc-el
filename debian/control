Source: markdown-toc-el
Section: lisp
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Nicholas D Steeves <sten@debian.org>
Build-Depends: debhelper-compat (= 13)
             , dh-elpa
             , elpa-el-mock
             , elpa-s
             , elpa-dash
             , elpa-markdown-mode
             , elpa-mocker
Rules-Requires-Root: no
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/emacsen-team/markdown-toc-el
Vcs-Git: https://salsa.debian.org/emacsen-team/markdown-toc-el.git
Homepage: https://github.com/ardumont/markdown-toc
Testsuite: autopkgtest-pkg-elpa

Package: elpa-markdown-toc
Architecture: all
Depends: ${elpa:Depends}
       , ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: Emacs TOC (table of contents) generator for markdown files
 Markdown-toc is a simple Emacs addon that parses a Markdown buffer
 and creates a handy table of contents.  It can also refresh an
 existing TOC to reflect updates to the body of the document.  When
 writing a standalone markdown document, when one cannot rely on
 external tools, rely on markdown-toc.  It provides greater control
 over output levels and headings than Pandoc's --toc.
